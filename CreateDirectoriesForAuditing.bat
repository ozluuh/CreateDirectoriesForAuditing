REM Product...........: Criador de Pastas de Arquivos
REM Author............: Luís Santos
REM Version...........: 1.0.0
REM Created_In........: 2018-11-16 14:37:11
REM Last_Modification.: 2018-11-17 01:57:30
REM Description.......: Script ideal para Áreas de Suporte Técnico e derivados onde são realizados atendimentos remotos
REM ..................: e há a necessidade de possuir arquivos/sessões salvos para análise dos casos.
::
::
::
@ECHO OFF & SETLOCAL ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION
TITLE Criador de Pastas
COLOR 03
SET MSG_LOOP=0
CLS

:LOOP
CLS
IF "%MSG_LOOP%" EQU 1 (
	ECHO Um nome para pasta deve ser informado para poder prosseguir
	ECHO.
	SET MSG_LOOP=0
	)

IF "%~1" NEQ "" (
		SET TMP=%~1
	) ELSE (
		SET /P TMP=Informe um nome para a pasta^>^>^> 
	)

IF "%TMP%" EQU "" (
	SET MSG_LOOP=1
	GOTO LOOP
	)
CLS
::
SET YY=%date:~-4%
SET MM=%date:~3,2%
SET DD=%date:~0,2%

:ESCOLHA_PASTA
ECHO Informe o local a salvar os arquivos: 
ECHO  [ Caminho Padrao: %USERPROFILE%\(Pasta Abaixo) ]
ECHO.
ECHO.
ECHO [1] Documentos
ECHO [2] Imagens
ECHO [3] Videos
ECHO [4] Outro
CHOICE /N /C 1234 /M ">>> "
	IF %ERRORLEVEL% EQU 4 SET PASTA_ESCOLHA=4
	IF %ERRORLEVEL% EQU 3 SET PASTA_ESCOLHA=3
	IF %ERRORLEVEL% EQU 2 SET PASTA_ESCOLHA=2
	IF %ERRORLEVEL% EQU 1 SET PASTA_ESCOLHA=1
GOTO TRANSICAO_PASTA

:TRANSICAO_PASTA
IF %PASTA_ESCOLHA% EQU 4 (
	ECHO.
	ECHO Informe o diretorio completo para salvar os arquivos:
	ECHO    Ex.: [%ProgamFiles%\Teste]
	ECHO.
	ECHO.
	SET /P P_TMP=^>^>^>
	GOTO DECISAO_PASTA
)

IF %PASTA_ESCOLHA% EQU 3 (
    SET P_TMP=%USERPROFILE%\Videos\Chamados
    GOTO DECISAO_PASTA
)

IF %PASTA_ESCOLHA% EQU 2 (
    SET P_TMP=%USERPROFILE%\Images\Clientes
    GOTO DECISAO_PASTA
)

IF %PASTA_ESCOLHA% EQU 3 (
    SET P_TMP=%USERPROFILE%\Documents\Clientes
    GOTO DECISAO_PASTA
)

:DECISAO_PASTA
CLS
ECHO Caminho Informado: [ %P_TMP% ]
ECHO.
ECHO Deseja:
ECHO [1] Prosseguir e Criar o diretorio
ECHO [2] Voltar e Alterar o diretorio
ECHO.
CHOICE /N /C 12 /M ">>> "
	IF %ERRORLEVEL% EQU 2 GOTO ESCOLHA_PASTA
	IF %ERRORLEVEL% EQU 1 SET PASTA_TMP=%P_TMP%

CALL :UPPER_CASE TMP
SET TMP > NUL
SET CLIENTE=%TMP%
CALL :CONVERTE_MES MM
CALL :MAKE_DIR "%PASTA_TMP%"

:MAKE_DIR
SET PASTA_ARQUIVOS=%~1
SET MSG_DIR=1
SET CAMINHO_COMPLETO="%PASTA_ARQUIVOS%\%YY%\%MM%-%EXT_MM%\"
IF NOT EXIST "%PASTA_ARQUIVOS%\%YY%\%MM%-%EXT_MM%\%CLIENTE%\%DD%" (
	   SET MSG=0
	   MKDIR "%PASTA_ARQUIVOS%\%YY%\%MM%-%EXT_MM%\%CLIENTE%\%DD%" > NUL
	)
GOTO QUIT

:QUIT
ECHO.
IF %MSG_DIR% EQU 1 (
		ECHO Pasta [ %CLIENTE%\%DD% ] existente no caminho informado [ %CAMINHO_COMPLETO% ]
	) ELSE (
		ECHO Pasta [ %CLIENTE%\%DD% ] foi criada com sucesso no seguinte diretorio [ %CAMINHO_COMPLETO% ]
		)
TIMEOUT /T 3 /NOBREAK
EXIT

:CONVERTE_MES
IF %MM% EQU 01 SET EXT_MM=JANEIRO
IF %MM% EQU 02 SET EXT_MM=FEVEREIRO
IF %MM% EQU 03 SET EXT_MM=MARCO
IF %MM% EQU 04 SET EXT_MM=ABRIL
IF %MM% EQU 05 SET EXT_MM=MAIO
IF %MM% EQU 06 SET EXT_MM=JUNHO
IF %MM% EQU 07 SET EXT_MM=JULHO
IF %MM% EQU 08 SET EXT_MM=AGOSTO
IF %MM% EQU 09 SET EXT_MM=SETEMBRO
IF %MM% EQU 10 SET EXT_MM=OUTUBRO
IF %MM% EQU 11 SET EXT_MM=NOVEMBRO
IF %MM% EQU 12 SET EXT_MM=DEZEMBRO
GOTO :EOF

:UPPER_CASE
REM Upper Case adaptado de https://gist.github.com/ijprest/1207818
for %%L IN (^ ^ A B C D E F G H I J K L M N O P Q R S T U V W X Y Z) DO SET %1=!%1:%%L=%%L!