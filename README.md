<!-- Last Modified: 2018-11-17 01:38:05 -->
# PT-BR

Script ideal para áreas de suporte técnico e derivados onde são realizados atendimentos remotos e é necessário salvar sessões, gravações e outros derivados para análises posteriores e/ou auditoria de informações.

Para melhor "automatizar" o Batch Script, o mesmo poderá ser parametrizado nas **Variáveis de Ambiente do Sistema** em:

    Painel de Controle > Sistema > Configurações Avançadas do Sistema

e executado a partir do comando

    Win + R


# EN-US

Ideal script for technical support areas and derivatives where remote calls are performed and it is necessary to save sessions, recordings and other derivatives for further analysis and / or audit of information.

To better "automate" the Batch Script, it can be parameterized in the **System Environment Variables** in:

     Control Panel > System > Advanced System Settings

and executed from the command

     Win + R